TARGET := plan.tex


all:
	latexmk $(TARGET)

clean:
	$(RM) *.aux *.log *.dvi *.fdb_latexmk  *.fls
